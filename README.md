Salut,

J'ai incorporé presque tout le contenu de **DD5** disponible sur [**AideDD,org**](https://www.aidedd.org/) dans un module pour [**Foundry VTT**](https://foundryvtt.com/).

Résumé :\
Ce module ajoute une 30ène de compendiums, des tables aléatoires et des entrées de journal remplies de liens permettant de naviguer comme dans un site web.\
Les compendiums "**Table des matières des joueurs**" et "**Table des matières du Maître [DM]**" sont respectivement les points de départ.\
**L'introduction** à DD5 et l'explication d'un jeu de röle.\
Une page de présentation de **AideDD** avec des liens utiles.\
**Les règlements, Création de personnage, Personnalité et historique, Caractéristiques, Options de personnalisation, Dons, Aventure, Combat, Incantations, Conditions.**\
**Races & sous-races** *(je vais probablement ajouter celles de Héros Dragons dans un compendium à part)*\
**Classes & sous-classes** *(je vais probablement ajouter celles de Héros Dragons dans un compendium à part)*\
**Capacités**, J'ai ajouté toutes les capacités de chaque classes & sous-classes, races & sous-races dans les pages.\
J'ai aussi ajouté des liens dans chaque classe vers les capacités disponibles.\
On peut même drag and drop les liens vers la fiche d'un personnage!\
*Par contre, il y a certaines capacités de sous-classes, races & sous-races que les traducteurs officiels n'ont pas encore ajoutées en "item/feet".\
Pour le moment, elles sont tout disponible dans le journal en format texte dans les classes/sous-classes et races/sous-races (Si je vois que les capacités "item/feet" n'arrivent pas assez vite, je vais probablement les crées).*\
**Équipements, Objets magiques.**  j'ai ajouté des liens vers les objets (drag & drop possible)\
**Liste des Sorts** classés d’A-Z, ou par niveau ou par classe & niveau.\
**Beaucoup d’aide pour MD** scénarios "clé en main", pleins d'aides de jeu, des suppléments\
**Liste des Monstres**\
**Règles optionnelles, WOTC, Unearthed Arcana, Sage Advice, D&D Beyond**\
Plus de **200 tables aléatoires** disponible sur aideDD (classes/sous-classes, races, historiques, babioles, beaucoup de tables pour MD)

***Il est conseillé de bloquer la visibilité des joueurs sur les compendiums avec la mention"[MD]" dans le titre (de 20-30 + la table des matières correspondante)***

*J'ai mis plusieurs liens externes vers le site de AideDD pour alléger le module et s'assurer que le jeu reste fluide!\
J'ai recopié & redimensionné chaque image que j'ai mis sur un serveur externe (600mbps de Upload)\
J'ai ajouté la source [AideDD.org] en bas de chacune des pages du journal.*

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Line_Divider_01.png>)

**Nom du module : En Francais SVP! (AideDD) [DD5]**\
**Version:** 0.8\
**Dernière mise a jour:** 30 juin 2020

Le module est disponible dans [**GitLab**](https://gitlab.com/sticklite13/en-francais-svp/-/raw/master/module/module.json)\
https://gitlab.com/sticklite13/en-francais-svp \
[Manifest](https://gitlab.com/sticklite13/en-francais-svp/-/raw/master/module/module.json): https://gitlab.com/sticklite13/en-francais-svp/-/raw/master/module/module.json

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Line_Divider_01.png>)

**Captures d'écran**

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Screenshot_01.jpg>)

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Screenshot_05_Foundry_demo.jpg>)

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Line_Divider_01.png>)

**Installations**

**Meilleur méthode**, par Manifest (simple et le module va être ajouté dans dans votre liste de Foundry pour les updates)\
[Manifest](https://gitlab.com/sticklite13/en-francais-svp/-/raw/master/module/module.json): https://gitlab.com/sticklite13/en-francais-svp/-/raw/master/module/module.json

1) Vous devez ouvrir Foundry et aller à la section des modules et appuyer sur "Installer un module"

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Installation_01.png>)

2) Vous devez saisir l'adresse du Manifest du module dans l'espace en bas et appuyer sur installation (tout ce fait automatique)\
Adresse du [Manifest](https://gitlab.com/sticklite13/en-francais-svp/-/raw/master/module/module.json): https://gitlab.com/sticklite13/en-francais-svp/-/raw/master/module/module.json

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Installation_02.png>)

3) C'est fait le module est dans votre liste, vous pouvez utilisé l'un des deux boutons pour les futures updates

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Installation_03.png>)

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Line_Divider_01.png>)

**24 juin 2020**

V0.3 Petit Update du module, j'ai ajouté des tables matières et bcp de liens entre les pages pour faciliter la navigation.\
Par exemple depuis la table de matières, on peut choisir une classe. Une fois dans la classe, on peut clicker sur le lien d'une capacité ou d'une sous-classe (subclasse) même chose pour les races, historiques, sort etc..

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Screenshot_02_Tables.jpg>)

**25 juin 2020**

V0.4 Créé des liens dans le livre vers les équipements/objets

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Screenshot_03_objets.jpg>)

**26 juin 2020**

V0.5 Créé des liens dans les classes vers les "items"

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Screenshot_04_Liens_Classes.jpg>)

**27 juin 2020**

V0.6 Ajouté [DD5] au titre du module et le module a été ajouté à la liste officiel de Foundry VTT\
V0.7 Changé quelques termes et titres

**30 juin 2020**

V0.8 Incorporations de plus de 200 tables aléatoires disponible sur AideDD (3 compendiums, dont un pour [MD])\
Ajouté aussi de nouvelles pages de journal pour les maîtres de donjons.

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Screenshot_06_Tables.jpg>)

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Line_Divider_01.png>)

Kaliss \
Jean-Francois \
Pour me contacter le plus simple est Discord: Kaliss#6644 \
Sticklite@hotmail.com \
Si vous avez envie de soutenir mon travail, n'hésitez pas à laisser un pourboire à mon paypal \
[Paypal](https://paypal.me/sticklite) : https://paypal.me/sticklite

![<Ligne>](<http://play.elderultima.com/Foundry_Fr/images/Divers/Line_Divider_01.png>)

This module brings the French translation of the DD5 content available on AideDD.org

On the website of AideDD.org it’s written:

*"AideDD is a website that talks about the medieval fantasy role-playing game Dungeons & Dragons since August 2000, and exclusively about its 5th edition since the end of 2013!\
Rules: Character creation, races, classes, backgrounds, equipment, feats, multiclassing, combat, magic, spells, conditions, creatures, magic items: all the rules you need to play in french.\
Tools: Alignment test, ready-to-play adventures, lots of usefull tools, extras, translations of articles from the Unearthed Arcana and Sage Advice sections of WotC and the D&D Beyond website."*

Kaliss \
Jean-Francois \
Contact me preferably on Discord Kaliss#6644 \
Or by mail Sticklite@hotmail.com \
If you feel like supporting my work, feel free to leave a tip at my paypal
[Paypal](https://paypal.me/sticklite) : https://paypal.me/sticklite